Ascii data file with some metadata in a header line and then paragraphs of data 
in plain text below.  Though not completely self describing.  Further info about the original data (oban.dat) is below:

The task is to read in the data and 1) make a geographic plot, 2) make a self describing file for future use.


"""
The data file called oban.dat is more complicated.

The first line is a header that defines the equally spaced lat/lon grid the data is on, as follows:

170.0 - the left longitude in a 0 to 360 convention
5.0     - the longitude grid spacing
41     - The number of longitude points
0.0   - the bottom latitude
5.0  - the latitude spacing
13   - the number of latitude points.

To the right of that is some metadata describing the data that follows.

After the head are blocks of data on the lon/lat grid for each month. For each month these are in blocks of three that contain

1) ugrid - the zonal component of the average tropical cyclone motion vector
2) vgird - the meridional component of the average TC motion vector
3) The intensity growth rate (kappa)

The data are written with longitude from west to east for each latitude, with latitude from N to S (each data line is at a single latitude for all longitudes).

"""
