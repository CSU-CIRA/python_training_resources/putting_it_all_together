# Solution:

Stages are broken into seperate files, the bulk of the work is done within data_utils.py. 
The raw data conversion is contained there and an xarray dataset is the main output.  
This is utilised to convert to netcdf or to plot the data from source.  

Some very cursory tests are included utilising `doctest` run these with 
`python -m doctest data_utils.py` .  Doctest is limited so these are just a quick example.
