""" Simple script to save converted dataset to netcdf
"""
import pathlib

import data_utils

NC_ENCODING = {'zlib': True, 'complevel': 1}


def create_encoding(dataset):
    encoding = {}
    for datavar in dataset:
        encoding[datavar] = NC_ENCODING.copy()
    return encoding


if __name__ == "__main__":
    raw_data, metadata = data_utils.read_raw_file(pathlib.Path('../oban.dat'))
    dataset = data_utils.convert_to_dataarray(raw_data, metadata)
    dataset.to_netcdf('oban.nc', encoding=create_encoding(dataset))
