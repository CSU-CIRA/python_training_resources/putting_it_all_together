import pathlib
import xarray as xr
import numpy as np
from collections import defaultdict


def decode_header_line(line: str) -> dict:
    """decode raw metadata from header line.  Assumes data stays static

    Parameters
    ----------
    line : str

    Returns
    -------
    dict

    >>> decode_header_line('170.0     5.0  41     0.0     5.0  13  basin=WH   u,v in m/s  cappa in 100*1/hr')
    {'first_lon': 170.0, 'delta_lon': 5.0, 'num_of_lon': 41, 'first_lat': 0.0, 'delta_lat': 5.0, 'num_of_lat': 13, 'basin': 'WH', 'units': 'u,v in m/s cappa in 100*1/hr'}

    """
    header_data = line.split()
    metadata = {}
    metadata['first_lon'] = float(header_data[0])
    metadata['delta_lon'] = float(header_data[1])
    metadata['num_of_lon'] = int(header_data[2])
    metadata['first_lat'] = float(header_data[3])
    metadata['delta_lat'] = float(header_data[4])
    metadata['num_of_lat'] = int(header_data[5])
    metadata['basin'] = header_data[6].split('=')[-1]
    metadata['units'] = ' '.join(header_data[7:])
    return metadata


def read_raw_file(filepath: pathlib.Path):
    """ Read plain text data file which contains paragraphs of data for 3 variables across multiple times
        Coord information is included in first line.  Units will have to be hard coded. 

    Parameters
    ----------
    filepath : pathlib.Path
    """
    with filepath.open() as f:
        header_line = next(f)
        metadata = decode_header_line(header_line)
        variable_name = 'unknown'
        data = {}
        for line in f:
            if not line.strip():  # if line is empty just ignore and move on
                continue
            if 'grid' in line:
                variable_name = line
                data[variable_name] = []
                continue
            data[variable_name].append(line.split())
    return data, metadata


def convert_to_dataarray(data: dict, metadata: dict):
    temp_array = defaultdict(list)
    for varname, dataarray in data.items():
        shortname = varname.split(',')[0]
        month = [
            int(varname.split()[-1]),
        ]
        coords = create_coord(metadata)
        coords.update({'month': month})

        dataarray = xr.DataArray([dataarray],
                                 name=shortname,
                                 dims=('month', 'latitude', 'longitude'),
                                 coords=coords).astype(float)

        temp_array[shortname].append(dataarray)

    ds = xr.Dataset()
    for name, data in temp_array.items():
        ds[name] = xr.concat(data, dim='month')
    return ds


def coord_array(start: float, delta: float, npoints: float) -> np.ndarray:
    """ helper function to calculate end point and generate linspace array

    Parameters
    ----------
    start : num
        fist grid point
    delta : num
        grid point spacing
    npoints : num
        number of points

    Returns
    -------
    np.ndarray

    Tests
    -----
    >>> coord_array(170.0, 5.0, 3)
    array([170., 175., 180.])

    >>> coord_array(-10.0, 5.0, 4)
    array([-10.,  -5.,   0.,   5.])
    """
    stop = start + delta * npoints
    return np.linspace(start, stop, npoints, endpoint=False)


def create_coord(metadata: dict) -> xr.DataArray:
    """Create dict of lat/lon coords for dataarray generation

    Parameters
    ----------
    metadata : dict
        continaing first lat/lon points, spacing and number of points

    Returns
    -------
    dict of latitude and longitude arrays of coords
    
    Tests
    -----
    >>> create_coord({'first_lon':0, 'delta_lon':1., 'num_of_lon':5,\
                      'first_lat':0, 'delta_lat':1., 'num_of_lat':5,})
    {'longitude': <xarray.DataArray 'longitude' (longitude: 5)>
    array([0., 1., 2., 3., 4.])
    Dimensions without coordinates: longitude
    Attributes:
        units:    degrees_east, 'latitude': <xarray.DataArray 'latitude' (latitude: 5)>
    array([4., 3., 2., 1., 0.])
    Dimensions without coordinates: latitude
    Attributes:
        units:    degrees_north}
    """
    coords = {
        'longitude':
        xr.DataArray(coord_array(
            metadata['first_lon'],
            metadata['delta_lon'],
            metadata['num_of_lon'],
        ),
                     dims='longitude',
                     name='longitude',
                     attrs={'units': 'degrees_east'}),
        'latitude':
        xr.DataArray(coord_array(
            metadata['first_lat'],
            metadata['delta_lat'],
            metadata['num_of_lat'],
        )[::-1],
                     dims='latitude',
                     name='latitude',
                     attrs={'units': 'degrees_north'}),
    }
    return coords


if __name__ == "__main__":
    raw_data, metadata = read_raw_file(pathlib.Path('oban.dat'))
    dataset = convert_to_dataarray(raw_data, metadata)
    print(dataset)
