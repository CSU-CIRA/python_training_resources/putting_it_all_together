import pathlib

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import xarray as xr

import data_utils


def open_dataset(filepath):
    if filepath.suffix == '.dat':
        raw_data, metadata = data_utils.read_raw_file(filepath)
        dataset = data_utils.convert_to_dataarray(raw_data, metadata)
    elif filepath.suffix == '.nc':
        dataset = xr.open_dataset(filepath)
    return dataset


def main(dataset):
    fig = plt.figure(figsize=(20, 10))
    ax = plt.axes(projection=ccrs.Mercator())

    ax.coastlines(zorder=1, color="slategrey", linewidth=2)

    levels = np.linspace(-3, 3, 100)
    print(dataset['cgrid'])
    contours = dataset['cgrid'].plot.contourf(levels=levels,
                                              zorder=0,
                                              transform=ccrs.PlateCarree(),
                                              cmap="seismic")
    # fig.colorbar(contours, label="cappa (100*1/hr)")
    lon2d, lat2d = np.meshgrid(dataset['longitude'], dataset['latitude'])
    ax.barbs(lon2d,
             lat2d,
             dataset['ugrid'].values,
             dataset['vgrid'].values,
             zorder=2,
             transform=ccrs.PlateCarree())

    ax.set_extent([-180, 10, 0, 60], crs=ccrs.PlateCarree())
    ax.gridlines(xlocs=dataset['longitude'],
                 ylocs=dataset['latitude'],
                 draw_labels=True,
                 linewidth=1,
                 color='gray',
                 alpha=1,
                 linestyle='--')
    plt.show()


def cli():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', type=pathlib.Path)
    parser.add_argument('-month', type=int, default=9)
    args = parser.parse_args()
    filepath = args.filepath
    month = args.month

    if not filepath.exists():
        raise FileNotFoundError(filepath)
    return filepath, month


if __name__ == "__main__":
    filepath, month = cli()
    dataset = open_dataset(filepath)
    main(dataset.sel(month=month))
