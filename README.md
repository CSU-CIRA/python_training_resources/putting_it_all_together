## Repo for short solutions to unique problems. 

Each directory will contain self-contained problems with methods to solve. 
Feel free to apply small modifications to existing solutions, or add new 
subdirectories for your own solutions.



